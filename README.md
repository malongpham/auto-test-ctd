# Auto test CTD

### Cài đặt composer
`composer install`

### Cấp quyền thư mục
`chmod -R 777`

## TEST
`composer test-build`

### Auto test

#### BUILD
`composer test-api-build`

#### CREATE
`composer test-api-create`

#### RUN GLOBAL TEST
`composer test-api-global`

#### RUN TEST
`composer test-api`

### Clean
`composer test-clean`

### Document
https://codeception.com/docs/reference/Commands

## GUIDE

### Thêm 1 endpoint

- Thêm endpoint vào `tests/auto.test.json`

- Nếu endpoint đã có thư mục thì chỉ cần khai báo ở file

- Nếu endpoint chưa có thư mục thì khai báo thư mục, file *Lưu ý: nếu muốn enndpoint chạy trước 1 endpoint có sẵn hay khai báo trước endpoint muốn chạy trước*

- Sau khi thêm chạy lệnh `composer test-api-create` 

- Chạy bản test sau khi thêm endpoint `composer test-api`

### Thêm 1 biến và sử dụng biến

- Thêm 1 biến tại `test/variables.php`

- Ở trong api endpoint muốn sử dụng biến thì gọi `{{ _.ten_bien }}`

- Sau khi thêm chạy lệnh `composer test-api-create`

- Chạy bản test sau khi thêm endpoint `composer test-api`
### Thêm 1 environment

- Thêm vào file `.env`