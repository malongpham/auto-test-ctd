<?php

namespace unit;

use \Codeception\Util\HttpCode;
use \Codeception\Scenario;
use UnitTester;

require_once(__DIR__ . '/../contants.php');
require_once(PATH_ROOT . '/variables.php');

class apiTestCest
{
    protected $successCode;
    protected $httpCode;
    protected $tester;
    protected $setting;
    protected $methods = [];
    protected $variables = [];

    function __construct()
    {
        $this->httpCode = new HttpCode;
        $this->successCode = $_ENV['SUCCESS_CODE'];
        $this->variables = $GLOBALS['variables'];
    }

    public function _before(UnitTester $I, Scenario $S)
    {
        $this->tester = $I;
        $this->setting = $S;
    }

    public function _readData($data)
    {
        $preg = "/\{{\s(\_.)(\w)+\s}}/";
        $variable = $GLOBALS["variables"];
        preg_match_all($preg, $data, $matches);
        foreach ($matches[0] as $arg) {
            $key = substr($arg, 5, -2);
            $rep = @$variable[trim($key)] . '$3';
            $read = preg_replace($preg, $rep, $data);
        }

        if (empty($read)) {
            $read = $data;
        }


        return $read;
    }

    public function _setData($data)
    {
        if (empty($data['data'])) {
            return;
        }
        if (!empty($data['data']['items'])) {
            foreach ($data['data']['items'] as $value) {
                foreach ($value as $kv => $v) {
                    if (!empty($this->variables[$kv])) {
                        file_put_contents(PATH_ROOT . "/variables.php", 'wb');
                        $this->variables[$kv] =  $v;
                        $variable_content_file = '<?php' . PHP_EOL . PHP_EOL;
                        foreach ($this->variables as $k => $var) {
                            $variable_content_file .= '$GLOBALS[\'variables\'][\'' . $k . '\'] = \'' . $var . '\';' . PHP_EOL;
                        }
                        file_put_contents(PATH_ROOT . "/variables.php", $variable_content_file);
                    } else {
                        continue;
                    }
                }
            }
        } else {
            foreach ($data['data'] as $value) {
                foreach ($value as $kv => $v) {
                    if (!empty($this->variables[$kv])) {
                        file_put_contents(PATH_ROOT . "/variables.php", 'wb');
                        $this->variables[$kv] =  $v;
                        $variable_content_file = '<?php' . PHP_EOL . PHP_EOL;
                        foreach ($this->variables as $k => $var) {
                            $variable_content_file .= '$GLOBALS[\'variables\'][\'' . $k . '\'] = \'' . $var . '\';' . PHP_EOL;
                        }
                        file_put_contents(PATH_ROOT . "/variables.php", $variable_content_file);
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}
