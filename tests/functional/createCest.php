<?php

namespace functional;

use \Codeception\Scenario;
use FunctionalTester;
require_once(__DIR__ . '/../contants.php');
require_once(PATH_ROOT . '/variables.php');

class createCest
{
    protected $tester;
    protected $setting;
    protected $variables = [];
    private $pathFile = PATH_ROOT . '/unit/test/';
    protected $dataInit;
    protected $project;
    protected $data;
    protected $variable;

    function __construct()
    {
        $this->variables = $GLOBALS['variables'];
    }

    public function _before(FunctionalTester $I, Scenario $S)
    {
        $this->tester = $I;
        $this->setting = $S;
    }

    public function create()
    {
        $this->dataInit = json_decode(
            file_get_contents($_ENV['FILE_TEST']),
            true
        );
        $this->project = $this->dataInit['project'];
        $this->data = $this->dataInit['resources'];
        $this->variable = $this->dataInit['variable'];

        $this->_createProject();
        $this->_createFolder();
        $this->_createFile();
        $this->_createVariable();
        $this->tester->wantTo("CREATE SUCCESS");
    }

    private function _createProject()
    {
        if (!file_exists($this->pathFile . $this->project)) {
            mkdir($this->pathFile . $this->project, 0777, true);
        }
    }

    private function _createFolder()
    {
        foreach ($this->data as $key => $value) {
            $folderName = $this->_convertToText(ucwords(strtolower($value['folder'])));
            if (!file_exists($this->pathFile . $this->project . '/' . $key . '.' . $folderName)) {
                mkdir($this->pathFile . $this->project . '/' . $key . '.' . $folderName, 0777, true);
            }
        }
    }

    private function _createFile()
    {
        foreach ($this->data as $key => $value) {
            $folderName = $this->_convertToText(ucwords(strtolower($value['folder'])));

            foreach ($value['file'] as $kf => $file) {
                if (file_exists($this->pathFile . $this->project . '/' . $key . '.' . $folderName)) {
                    $fileName = $this->_convertToText(ucwords(strtolower($file['name'])));
                    $method = ucwords(strtolower($file['method']));
                    $functionMethod = $file['method'];
                    $headers = $file['headers'];
                    $body = $file['body'];
                    $baseUrl = $file['url'];
                    $notify = strtoupper($file['name']);

                    fopen($this->pathFile . $this->project . '/' . $key . '.' . $folderName . '/' . $kf . $fileName . $method . 'Cest' . '.php', 'wb');
                    $file = $this->pathFile . $this->project . '/' . $key . '.' . $folderName . '/' . $kf . $fileName . $method . 'Cest' . '.php';
                    $spc = '    ';
                    $br = PHP_EOL;
                    $current = file_get_contents($file);
                    $current .= '<?php' . $br . $br;
                    $current .= 'namespace ' . $folderName . ';' . $br . $br;
                    $current .= 'use \unit\apiTestCest as apiTest;' . $br . $br;
                    $current .= 'class ' . $fileName . $kf . $method . 'Cest extends apiTest' . $br;
                    $current .= '{' . $br;
                    $current .= $spc . 'public function ' . $fileName . $method . '()' . $br;
                    $current .= $spc . '{' . $br;
                    $current .= $spc . $spc . '$url = "' . $baseUrl . '";' . $br;
                    switch($functionMethod) {
                        case 'GET':
                            if (!empty($headers)) {
                                foreach ($headers as $header) {
                                    $current .= $spc . $spc . '$this->tester->haveHttpHeader($this->_readData("' . $header['name'] . '"), $this->_readData("' . $header['value'] . '"));' . $br;
                                }
                            }
                            $current .= $spc . $spc . '$this->tester->sendGet($this->_readData($url));' . $br;
                            break;
                        case 'POST':
                            if (!empty($headers)) {
                                foreach ($headers as $header) {
                                    $current .= $spc . $spc . '$this->tester->haveHttpHeader($this->_readData("' . $header['name'] . '"), $this->_readData("' . $header['value'] . '"));' . $br;
                                }
                            }
                            if (!empty($body)) {
                                if (isset($body['text'])) {
                                    $current .= $spc . $spc . '$body = json_decode(\'' . preg_replace("/[\r\n]*(\s)/","",@$body["text"]) . '\', true);' . $br;
                                } else {
                                    $current .= $spc . $spc . '$body = "";' . $br;
                                }
                            }
                            $current .= $spc . $spc . '$this->tester->sendPost($this->_readData($url), $body);' . $br;
                            break;
                        case 'PUT':
                            if (!empty($headers)) {
                                foreach ($headers as $header) {
                                    $current .= $spc . $spc . '$this->tester->haveHttpHeader($this->_readData("' . $header['name'] . '"), $this->_readData("' . $header['value'] . '"));' . $br;
                                }
                            }
                            if (!empty($body)) {
                                if (isset($body['text'])) {
                                    $current .= $spc . $spc . '$body = json_decode(\'' . preg_replace("/[\r\n]*(\s)/","",@$body["text"]) . '\', true);' . $br;
                                } else {
                                    $current .= $spc . $spc . '$body = "";' . $br;
                                }
                            }
                            $current .= $spc . $spc . '$this->tester->sendPost($this->_readData($url), $body);' . $br;
                            break;
                        case 'DELETE':
                            if (!empty($headers)) {
                                foreach ($headers as $header) {
                                    $current .= $spc . $spc . '$this->tester->haveHttpHeader($this->_readData("' . $header['name'] . '"), $this->_readData("' . $header['value'] . '"));' . $br;
                                }
                            }
                            $current .= $spc . $spc . '$this->tester->sendDelete($this->_readData($url));' . $br;
                            break;
                    }
                    $current .= $spc . $spc . '$response = $this->tester->grabResponse();' . $br;
                    $current .= $spc . $spc . '$response = json_decode($response, true);' . $br;
                    $current .= $spc . $spc . '$this->_setData($response);' . $br;
                    $current .= $spc . $spc . '$this->tester->seeResponseIsJson();' . $br;
                    $current .= $spc . $spc . '$this->tester->seeResponseCodeIs($this->httpCode::OK);' . $br;
                    $current .= $spc . $spc . '$this->tester->dontSeeResponseCodeIs($this->httpCode::BAD_REQUEST);' . $br;
                    $current .= $spc . $spc . '$this->tester->dontSeeResponseCodeIs($this->httpCode::FORBIDDEN);' . $br;
                    $current .= $spc . $spc . '$this->tester->dontSeeResponseCodeIs($this->httpCode::NOT_FOUND);' . $br;
                    $current .= $spc . $spc . '$this->tester->wantTo("' . $notify . '");' . $br;
                    $current .= $spc . '}' . $br;
                    $current .= '}' . $br;
                    file_put_contents($file, $current);
                }
            }
        }
    }

    private function _createVariable()
    {
        file_put_contents(PATH_ROOT . "/variables.php", 'wb');
        $GLOBALS["variables"] = $this->variable;
        $variable_content_file = '<?php' . PHP_EOL . PHP_EOL;
        foreach ($this->variable as $k => $var) {
            $variable_content_file .= '$GLOBALS[\'variables\'][\''.$k.'\'] = \''.$var.'\';' . PHP_EOL;
        }
        file_put_contents(PATH_ROOT . "/variables.php", $variable_content_file);
    }

    private function _convertToText($str)
    {
        $convert = preg_replace('/[^A-Za-z]/', '', $str);
        return $convert;
    }
}
