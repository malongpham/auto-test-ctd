<?php

namespace acceptance;

use \Codeception\Scenario;
use AcceptanceTester;

class buildCest
{
    protected $data = [];
    protected $project;
    protected $group;
    protected $tester;
    protected $setting;
    protected $dataTest;

    function __construct()
    {
        $this->dataTest = json_decode(
            file_get_contents($_ENV['FILE_PATH']), 
            true
        );
    }

    public function _before(AcceptanceTester $I, Scenario $S)
    {
        $this->tester = $I;
        $this->setting = $S;
    }

    public function build()
    {
        $this->_createEmptyJsonFileTest();
        $this->_buildProject();
        $this->_buildGroup();
        $this->_buildFile();
        $this->_buildVariable();
        file_put_contents($_ENV['FILE_TEST'], json_encode($this->data));
        $this->tester->wantTo("BUILD SUCCESS");
    }

    private function _buildProject()
    {
        foreach ($this->dataTest['resources'] as $value) {
            if ($value['_type'] == 'workspace') {
                $this->data['project'] = $value['name'];
                $dataProject = 'resources';
                $this->data[$dataProject] = [];
            }
        }
        return $this->project = $dataProject;
    }

    private function _buildGroup()
    {
        foreach ($this->dataTest['resources'] as $value) {
            if ($value['_type'] == 'request_group') {
                $group = $value['name'];
                $this->data[$this->project][] = [
                    'folder' => $group,
                ];
            }
        }
    }

    private function _buildFile()
    {
        $data = [];
        foreach ($this->dataTest['resources'] as $key => $value) {
            if ($value['_type'] == 'request') {
                $key_id = array_search($value['parentId'], array_column($this->dataTest['resources'], '_id'));
                $file = $value['name'];
                $data[$key] = [
                    'id' => $value['_id'],
                    'parentId' => $value['parentId'],
                    'name' => $file,
                    'url' => $value['url'],
                    'method' => $value['method'],
                    'metaSort' => $value['metaSortKey'],
                    'body' => $this->_getDataRequest($value['body']),
                    'parameters' => $this->_getDataRequest($value['parameters']),
                    'headers' => $this->_getDataRequest($value['headers']),
                ];
                $group = $this->dataTest['resources'][$key_id]['name'];
                $key_id_group = array_search($group, array_column($this->data[$this->project], 'folder'));
                $this->data[$this->project][$key_id_group]['file'][] =  $data[$key];
            }
        }
    }

    private function _buildVariable()
    {
        foreach ($this->dataTest['resources'] as $value) {
            if ($value['_type'] == 'environment') {
                $this->data['variable'] = $value['data'];
            }
        }
    }

    private function _createEmptyJsonFileTest()
    {
        $jsonString = file_get_contents($_ENV['FILE_TEST']);
        $data = json_decode($jsonString, true);
        $data = [];
        $newJsonString = json_encode($data);
        file_put_contents($_ENV['FILE_TEST'], $newJsonString);
    }

    private function _getDataRequest($body)
    {
        $data = [];   
        foreach ($body as $key => $bd) {
            $data[$key] = $bd;
        }
        return $data;
    }
}
